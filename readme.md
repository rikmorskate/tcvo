# Emixion default installation
## Installing

First clone the repository and the submodules (If you did used git clone without --recurse-submodules, use the command ``git submodule update --recursive`` in the root folder).
```
git clone git@bitbucket.org:emixiondevelopment/emixion-default-installation.git --recurse-submodules
```

After cloning the repository, run composer install to install the latest packages and libraries.
```
composer install
```

When composer install is ready, run the follow command and fill in all the information that will be asked.
```
php artisan asgard:install
```

After the CMS is installed, run our own setup command, to setup all the default settings we use.
```
php artisan emixion:setup
```

Now you've successfully installed Emixion Default Installation (Emixion CMS), enjoy!

## Usage
### Backend login
To log in into the backend, enter ``/emixion`` behind the root url (example: http://localhost/emixion) and log in with the information you've entered during the `php artisan asgard::install` command/installation.

### Enable locale in url (default off)
To enable the language prefix/locale in the url, go to and open ``/config/laravellocalization.php`` and set ``'hideDefaultLocaleInURL' => true`` to ``false``.