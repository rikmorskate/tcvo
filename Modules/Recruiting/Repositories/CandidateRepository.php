<?php namespace Modules\Recruiting\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface CandidateRepository extends BaseRepository
{
}
