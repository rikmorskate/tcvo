<?php namespace Modules\Recruiting\Repositories\Eloquent;

use Modules\Recruiting\Repositories\OrganizationRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentOrganizationRepository extends EloquentBaseRepository implements OrganizationRepository
{
}
