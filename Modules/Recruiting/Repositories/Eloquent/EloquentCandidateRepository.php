<?php namespace Modules\Recruiting\Repositories\Eloquent;

use Modules\Recruiting\Repositories\CandidateRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentCandidateRepository extends EloquentBaseRepository implements CandidateRepository
{
}
