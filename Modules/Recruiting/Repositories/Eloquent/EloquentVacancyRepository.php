<?php namespace Modules\Recruiting\Repositories\Eloquent;

use Modules\Recruiting\Repositories\VacancyRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentVacancyRepository extends EloquentBaseRepository implements VacancyRepository
{
}
