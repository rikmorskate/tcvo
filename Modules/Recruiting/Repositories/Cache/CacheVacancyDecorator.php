<?php namespace Modules\Recruiting\Repositories\Cache;

use Modules\Recruiting\Repositories\VacancyRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheVacancyDecorator extends BaseCacheDecorator implements VacancyRepository
{
    public function __construct(VacancyRepository $vacancy)
    {
        parent::__construct();
        $this->entityName = 'recruiting.vacancies';
        $this->repository = $vacancy;
    }
}
