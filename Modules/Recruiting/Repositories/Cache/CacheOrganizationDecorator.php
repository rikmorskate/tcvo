<?php namespace Modules\Recruiting\Repositories\Cache;

use Modules\Recruiting\Repositories\OrganizationRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheOrganizationDecorator extends BaseCacheDecorator implements OrganizationRepository
{
    public function __construct(OrganizationRepository $organization)
    {
        parent::__construct();
        $this->entityName = 'recruiting.organizations';
        $this->repository = $organization;
    }
}
