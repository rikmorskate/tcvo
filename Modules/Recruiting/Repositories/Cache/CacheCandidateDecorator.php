<?php namespace Modules\Recruiting\Repositories\Cache;

use Modules\Recruiting\Repositories\CandidateRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheCandidateDecorator extends BaseCacheDecorator implements CandidateRepository
{
    public function __construct(CandidateRepository $candidate)
    {
        parent::__construct();
        $this->entityName = 'recruiting.candidates';
        $this->repository = $candidate;
    }
}
