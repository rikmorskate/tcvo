<?php namespace Modules\Recruiting\Providers;

use Illuminate\Support\ServiceProvider;

class RecruitingServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Recruiting\Repositories\VacancyRepository',
            function () {
                $repository = new \Modules\Recruiting\Repositories\Eloquent\EloquentVacancyRepository(new \Modules\Recruiting\Entities\Vacancy());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Recruiting\Repositories\Cache\CacheVacancyDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Recruiting\Repositories\CandidateRepository',
            function () {
                $repository = new \Modules\Recruiting\Repositories\Eloquent\EloquentCandidateRepository(new \Modules\Recruiting\Entities\Candidate());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Recruiting\Repositories\Cache\CacheCandidateDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Recruiting\Repositories\OrganizationRepository',
            function () {
                $repository = new \Modules\Recruiting\Repositories\Eloquent\EloquentOrganizationRepository(new \Modules\Recruiting\Entities\Organization());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Recruiting\Repositories\Cache\CacheOrganizationDecorator($repository);
            }
        );
// add bindings



    }
}
