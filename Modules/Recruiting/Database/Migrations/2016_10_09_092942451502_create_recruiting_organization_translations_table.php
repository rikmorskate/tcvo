<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecruitingOrganizationTranslationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recruiting__organization_trans', function(Blueprint $table) {
			$table->engine = 'InnoDB';
            $table->increments('id');
			$table->text('details');

            $table->integer('organization_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['organization_id', 'locale']);
            $table->foreign('organization_id')->references('id')->on('recruiting__organizations')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recruiting__organization_translations');
	}
}
