<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecruitingVacancyTranslationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recruiting__vacancy_translations', function(Blueprint $table) {
			$table->engine = 'InnoDB';
            $table->increments('id');
			$table->string('title');
			$table->string('slug');
			$table->text('details');
			$table->text('requirements');
			$table->string('education');

			$table->string('meta_title');
			$table->string('meta_description');

            $table->integer('vacancy_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['vacancy_id', 'locale']);
            $table->foreign('vacancy_id')->references('id')->on('recruiting__vacancies')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recruiting__vacancy_translations');
	}
}
