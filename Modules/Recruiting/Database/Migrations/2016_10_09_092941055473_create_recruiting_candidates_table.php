<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecruitingCandidatesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('recruiting__candidates', function(Blueprint $table) {
			$table->engine = 'InnoDB';
            $table->increments('id');
			$table->string('name');
			$table->string('email_address');
			$table->string('phone_number');

			$table->string('street_name');
			$table->string('street_number');
			$table->string('street_number_suffix');
			$table->string('minor_municipality');
			$table->string('governing_district', 3);
			$table->string('postal_area');
			$table->string('country', 2)->default('nl');

			$table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('recruiting__candidates');
	}
}
