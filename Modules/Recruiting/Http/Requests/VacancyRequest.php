<?php namespace Modules\Products\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class VacancyRequest extends BaseFormRequest
{
    protected $translationsAttributesKey = 'recruiting::vacancies.title.';

    public function rules()
    {
        return[
        ];
    }

    public function translationRules()
    {
        return [
            'title' => 'required|max:255',
            'slug' => 'required|max:255',
            'type' => 'required',
//            'type' => 'exists:products__types,id'
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return[
        ];
    }

    public function translationMessages()
    {
        return [
        ];
    }
}
