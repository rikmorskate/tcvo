<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/recruiting'], function (Router $router) {
    $router->bind('vacancy', function ($id) {
        return app('Modules\Recruiting\Repositories\VacancyRepository')->find($id);
    });
    $router->get('vacancies', [
        'as' => 'admin.recruiting.vacancy.index',
        'uses' => 'VacancyController@index',
        'middleware' => 'can:recruiting.vacancies.index'
    ]);
    $router->get('vacancies/create', [
        'as' => 'admin.recruiting.vacancy.create',
        'uses' => 'VacancyController@create',
        'middleware' => 'can:recruiting.vacancies.create'
    ]);
    $router->post('vacancies', [
        'as' => 'admin.recruiting.vacancy.store',
        'uses' => 'VacancyController@store',
        'middleware' => 'can:recruiting.vacancies.store'
    ]);
    $router->get('vacancies/{vacancy}/edit', [
        'as' => 'admin.recruiting.vacancy.edit',
        'uses' => 'VacancyController@edit',
        'middleware' => 'can:recruiting.vacancies.edit'
    ]);
    $router->put('vacancies/{vacancy}', [
        'as' => 'admin.recruiting.vacancy.update',
        'uses' => 'VacancyController@update',
        'middleware' => 'can:recruiting.vacancies.update'
    ]);
    $router->delete('vacancies/{vacancy}', [
        'as' => 'admin.recruiting.vacancy.destroy',
        'uses' => 'VacancyController@destroy',
        'middleware' => 'can:recruiting.vacancies.destroy'
    ]);

    $router->bind('candidate', function ($id) {
        return app('Modules\Recruiting\Repositories\CandidateRepository')->find($id);
    });
    $router->get('candidates', [
        'as' => 'admin.recruiting.candidate.index',
        'uses' => 'CandidateController@index',
        'middleware' => 'can:recruiting.candidates.index'
    ]);
    $router->get('candidates/create', [
        'as' => 'admin.recruiting.candidate.create',
        'uses' => 'CandidateController@create',
        'middleware' => 'can:recruiting.candidates.create'
    ]);
    $router->post('candidates', [
        'as' => 'admin.recruiting.candidate.store',
        'uses' => 'CandidateController@store',
        'middleware' => 'can:recruiting.candidates.store'
    ]);
    $router->get('candidates/{candidate}/edit', [
        'as' => 'admin.recruiting.candidate.edit',
        'uses' => 'CandidateController@edit',
        'middleware' => 'can:recruiting.candidates.edit'
    ]);
    $router->put('candidates/{candidate}', [
        'as' => 'admin.recruiting.candidate.update',
        'uses' => 'CandidateController@update',
        'middleware' => 'can:recruiting.candidates.update'
    ]);
    $router->delete('candidates/{candidate}', [
        'as' => 'admin.recruiting.candidate.destroy',
        'uses' => 'CandidateController@destroy',
        'middleware' => 'can:recruiting.candidates.destroy'
    ]);
    
    $router->bind('organization', function ($id) {
        return app('Modules\Recruiting\Repositories\OrganizationRepository')->find($id);
    });
    $router->get('organizations', [
        'as' => 'admin.recruiting.organization.index',
        'uses' => 'OrganizationController@index',
        'middleware' => 'can:recruiting.organizations.index'
    ]);
    $router->get('organizations/create', [
        'as' => 'admin.recruiting.organization.create',
        'uses' => 'OrganizationController@create',
        'middleware' => 'can:recruiting.organizations.create'
    ]);
    $router->post('organizations', [
        'as' => 'admin.recruiting.organization.store',
        'uses' => 'OrganizationController@store',
        'middleware' => 'can:recruiting.organizations.store'
    ]);
    $router->get('organizations/{organization}/edit', [
        'as' => 'admin.recruiting.organization.edit',
        'uses' => 'OrganizationController@edit',
        'middleware' => 'can:recruiting.organizations.edit'
    ]);
    $router->put('organizations/{organization}', [
        'as' => 'admin.recruiting.organization.update',
        'uses' => 'OrganizationController@update',
        'middleware' => 'can:recruiting.organizations.update'
    ]);
    $router->delete('organizations/{organization}', [
        'as' => 'admin.recruiting.organization.destroy',
        'uses' => 'OrganizationController@destroy',
        'middleware' => 'can:recruiting.organizations.destroy'
    ]);
// append



});
