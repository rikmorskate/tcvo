<?php namespace Modules\Recruiting\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Recruiting\Entities\Vacancy;
use Modules\Recruiting\Repositories\VacancyRepository;
use Modules\Recruiting\Transformers\VacancyTransformer;
use Pingpong\Modules\Routing\Controller;
use Modules\Pager\Facades\PageSettings;
use Modules\Globalsetting\Facades\Settings;
use Dimsav\Translatable\TranslatableServiceProvider;
use Emixion\Transformers\Transformers;

class VacancyController extends Controller
{
    /**
     * @var VacancyRepository
     */
    private $vacancy;

    public function __construct(Transformers $response, Request $request)
    {
        $this->response = $response;
        $this->response->setSerializer(new \Modules\Products\Serializers\JsonApiSerializer());

        if ($include = $request->input('include')) {
            $this->response->parseIncludes($include);
        }

        if ($accept_language = substr(\Request::server('HTTP_ACCEPT_LANGUAGE'), 0, 2)) {
            \App::setLocale($accept_language);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $filters = $request->get('filter');

        dd($filters);

        $vacancies = Vacancy::where('deleted_at', null);

        if (!empty($filters)) {
            foreach ($filters AS $filter => $option) {
                if ($filter == 'title') {
                    $vacancies->whereTranslationLike($filter, '%'.$option.'%');
                } elseif ($filter == 'education') {
                    if (strpos($option, ',') !== false) {
                        $vacancies->whereInTranslation($filter, explode(',', $option));
                    }
                } elseif ($filter == 'type') {
                    if (strpos($option, ',') !== false) {
                        $vacancies->whereIn($filter, explode(',', $option));
                    }
                }
            }
        }

//        dd($vacancies->paginate(2));

        return $this->response->paginate($vacancies->paginate(2), new VacancyTransformer(), 'vacancies');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function show($vacancy)
    {
        dd($vacancy);
//        \App::setLocale('NL');
        $vacancies = $this->vacancy->all();

//        $resource = new Fractal\Resource\Item($book, new BookTransformer);

        return response()->json($vacancies);

//        return $vacancies->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('recruiting::admin.vacancies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->vacancy->create($request->all());

        flash()->success(trans('core::core.messages.resource created', ['name' => trans('recruiting::vacancies.title.vacancies')]));

        return redirect()->route('admin.recruiting.vacancy.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Vacancy $vacancy
     * @return Response
     */
    public function edit(Vacancy $vacancy)
    {
        return view('recruiting::admin.vacancies.edit', compact('vacancy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Vacancy $vacancy
     * @param  Request $request
     * @return Response
     */
    public function update(Vacancy $vacancy, Request $request)
    {
        $this->vacancy->update($vacancy, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('recruiting::vacancies.title.vacancies')]));

        return redirect()->route('admin.recruiting.vacancy.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Vacancy $vacancy
     * @return Response
     */
    public function destroy(Vacancy $vacancy)
    {
        $this->vacancy->destroy($vacancy);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('recruiting::vacancies.title.vacancies')]));

        return redirect()->route('admin.recruiting.vacancy.index');
    }
}
