<?php namespace Modules\Recruiting\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Recruiting\Entities\Organization;
use Modules\Recruiting\Repositories\OrganizationRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class OrganizationController extends AdminBaseController
{
    /**
     * @var OrganizationRepository
     */
    private $organization;

    public function __construct(OrganizationRepository $organization)
    {
        parent::__construct();

        $this->organization = $organization;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$organizations = $this->organization->all();

        return view('recruiting::admin.organizations.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('recruiting::admin.organizations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $organization = $this->organization->create($request->all());

//        dump($request->all());
//        dd($organization);

        flash()->success(trans('core::core.messages.resource created', ['name' => trans('recruiting::organizations.title.organizations')]));

        return redirect()->route('admin.recruiting.organization.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Organization $organization
     * @return Response
     */
    public function edit(Organization $organization)
    {
        return view('recruiting::admin.organizations.edit', compact('organization'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Organization $organization
     * @param  Request $request
     * @return Response
     */
    public function update(Organization $organization, Request $request)
    {
        $this->organization->update($organization, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('recruiting::organizations.title.organizations')]));

        return redirect()->route('admin.recruiting.organization.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Organization $organization
     * @return Response
     */
    public function destroy(Organization $organization)
    {
        $this->organization->destroy($organization);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('recruiting::organizations.title.organizations')]));

        return redirect()->route('admin.recruiting.organization.index');
    }
}
