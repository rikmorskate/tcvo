<?php namespace Modules\Recruiting\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Recruiting\Entities\Vacancy;
use Modules\Recruiting\Entities\Organization;
use Modules\Recruiting\Repositories\VacancyRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class VacancyController extends AdminBaseController
{
    /**
     * @var VacancyRepository
     */
    private $vacancy;

    public function __construct(VacancyRepository $vacancy)
    {
        parent::__construct();

        $this->vacancy = $vacancy;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $vacancies = $this->vacancy->all();

        return view('recruiting::admin.vacancies.index', compact('vacancies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('recruiting::admin.vacancies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $vacancy = $this->vacancy->create($request->all());

        if ($organization = Organization::find($request->get('organization'))) {
            $vacancy->organization()->associate($organization)->save();
        }

        flash()->success(trans('core::core.messages.resource created', ['name' => trans('recruiting::vacancies.title.vacancies')]));

        return redirect()->route('admin.recruiting.vacancy.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Vacancy $vacancy
     * @return Response
     */
    public function edit(Vacancy $vacancy)
    {
        return view('recruiting::admin.vacancies.edit', compact('vacancy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Vacancy $vacancy
     * @param  Request $request
     * @return Response
     */
    public function update(Vacancy $vacancy, Request $request)
    {
        $this->vacancy->update($vacancy, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('recruiting::vacancies.title.vacancies')]));

        return redirect()->route('admin.recruiting.vacancy.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Vacancy $vacancy
     * @return Response
     */
    public function destroy(Vacancy $vacancy)
    {
        $this->vacancy->destroy($vacancy);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('recruiting::vacancies.title.vacancies')]));

        return redirect()->route('admin.recruiting.vacancy.index');
    }
}
