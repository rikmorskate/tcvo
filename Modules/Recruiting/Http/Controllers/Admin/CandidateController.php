<?php namespace Modules\Recruiting\Http\Controllers\Admin;

use Laracasts\Flash\Flash;
use Illuminate\Http\Request;
use Modules\Recruiting\Entities\Candidate;
use Modules\Recruiting\Repositories\CandidateRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class CandidateController extends AdminBaseController
{
    /**
     * @var CandidateRepository
     */
    private $candidate;

    public function __construct(CandidateRepository $candidate)
    {
        parent::__construct();

        $this->candidate = $candidate;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$candidates = $this->candidate->all();

        return view('recruiting::admin.candidates.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('recruiting::admin.candidates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->candidate->create($request->all());

        flash()->success(trans('core::core.messages.resource created', ['name' => trans('recruiting::candidates.title.candidates')]));

        return redirect()->route('admin.recruiting.candidate.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Candidate $candidate
     * @return Response
     */
    public function edit(Candidate $candidate)
    {
        return view('recruiting::admin.candidates.edit', compact('candidate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Candidate $candidate
     * @param  Request $request
     * @return Response
     */
    public function update(Candidate $candidate, Request $request)
    {
        $this->candidate->update($candidate, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('recruiting::candidates.title.candidates')]));

        return redirect()->route('admin.recruiting.candidate.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Candidate $candidate
     * @return Response
     */
    public function destroy(Candidate $candidate)
    {
        $this->candidate->destroy($candidate);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('recruiting::candidates.title.candidates')]));

        return redirect()->route('admin.recruiting.candidate.index');
    }
}
