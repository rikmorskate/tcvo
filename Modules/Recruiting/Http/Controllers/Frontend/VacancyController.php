<?php namespace Modules\Recruiting\Http\Controllers\Frontend;

use Modules\Recruiting\Entities\Vacancy;
use Modules\Recruiting\Repositories\VacancyRepository;
use Pingpong\Modules\Routing\Controller;
use Modules\Pager\Facades\PageSettings;
use Modules\Globalsetting\Facades\Settings;

class VacancyController extends Controller
{
    /**
     * @var VacancyRepository
     */
    private $vacancy;

    public function __construct(VacancyRepository $vacancy)
    { 
        $this->vacancy = $vacancy;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $vacancies = Vacancy::take(4)->orderBy('created_at', 'DESC')->get();
        $lang = locale();

//        dd($vacancies[0]->title);

        return view('recruiting.vacancies.index.' . PageSettings::getDisplayView(locale(), 'index'), compact('vacancies', 'lang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('recruiting::admin.vacancies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->vacancy->create($request->all());

        flash()->success(trans('core::core.messages.resource created', ['name' => trans('recruiting::vacancies.title.vacancies')]));

        return redirect()->route('admin.recruiting.vacancy.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Vacancy $vacancy
     * @return Response
     */
    public function edit(Vacancy $vacancy)
    {
        return view('recruiting::admin.vacancies.edit', compact('vacancy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Vacancy $vacancy
     * @param  Request $request
     * @return Response
     */
    public function update(Vacancy $vacancy, Request $request)
    {
        $this->vacancy->update($vacancy, $request->all());

        flash()->success(trans('core::core.messages.resource updated', ['name' => trans('recruiting::vacancies.title.vacancies')]));

        return redirect()->route('admin.recruiting.vacancy.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Vacancy $vacancy
     * @return Response
     */
    public function destroy(Vacancy $vacancy)
    {
        $this->vacancy->destroy($vacancy);

        flash()->success(trans('core::core.messages.resource deleted', ['name' => trans('recruiting::vacancies.title.vacancies')]));

        return redirect()->route('admin.recruiting.vacancy.index');
    }
}
