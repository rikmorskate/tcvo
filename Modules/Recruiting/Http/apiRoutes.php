<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group(['prefix' => '/recruiting'], function (Router $router) {
    $router->bind('vacancy', function ($id) {
        return app('Modules\Recruiting\Repositories\VacancyRepository')->find($id);
    });

    $router->get('vacancies', [
        'as' => 'api.vacancies.vacancy.index',
        'uses' => 'VacancyController@index',
//        'middleware' => 'can:vacancies.vacancy.index'
    ]);

    $router->get('vacancies/{vacancy}', [
        'as' => 'api.vacancies.vacancy.show',
        'uses' => 'VacancyController@show',
//        'middleware' => 'can:vacancies.vacancy.show'
    ]);

    $router->get('vacancies', 'VacancyController@index');

//    $router->get('vacancies/create', [
//        'as' => 'api.recruiting.vacancy.create',
//        'uses' => 'VacancyController@create',
//        'middleware' => 'can:recruiting.vacancies.create'
//    ]);
//    $router->post('vacancies', [
//        'as' => 'api.recruiting.vacancy.store',
//        'uses' => 'VacancyController@store',
//        'middleware' => 'can:recruiting.vacancies.store'
//    ]);
//    $router->get('vacancies/{vacancy}/edit', [
//        'as' => 'api.recruiting.vacancy.edit',
//        'uses' => 'VacancyController@edit',
//        'middleware' => 'can:recruiting.vacancies.edit'
//    ]);
//    $router->put('vacancies/{vacancy}', [
//        'as' => 'api.recruiting.vacancy.update',
//        'uses' => 'VacancyController@update',
//        'middleware' => 'can:recruiting.vacancies.update'
//    ]);
//    $router->delete('vacancies/{vacancy}', [
//        'as' => 'api.recruiting.vacancy.destroy',
//        'uses' => 'VacancyController@destroy',
//        'middleware' => 'can:recruiting.vacancies.destroy'
//    ]);

});