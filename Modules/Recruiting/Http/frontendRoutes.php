<?php

Breadcrumbs::register('recruiting::vacancy', function($breadcrumbs, $vacancy)
{
    $breadcrumbs->parent('pager::page');
    $breadcrumbs->push($vacancy->title, url($vacancy->slug));
});