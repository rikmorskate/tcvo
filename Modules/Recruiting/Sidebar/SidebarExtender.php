<?php namespace Modules\Recruiting\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Contracts\Authentication;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('recruiting::abcs.title.abcs'), function (Item $item) {
                $item->icon('fa fa-briefcase');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('recruiting::vacancies.title.vacancies'), function (Item $item) {
                    $item->icon('fa fa-briefcase');
                    $item->weight(1);
                    $item->append('admin.recruiting.vacancy.create');
                    $item->route('admin.recruiting.vacancy.index');
                    $item->authorize(
                        $this->auth->hasAccess('recruiting.vacancies.index')
                    );
                });
                $item->item(trans('recruiting::organizations.title.organizations'), function (Item $item) {
                    $item->icon('fa fa-building-o');
                    $item->weight(2);
                    $item->append('admin.recruiting.organization.create');
                    $item->route('admin.recruiting.organization.index');
                    $item->authorize(
                        $this->auth->hasAccess('recruiting.organizations.index')
                    );
                });
                $item->item(trans('recruiting::candidates.title.candidates'), function (Item $item) {
                    $item->icon('fa fa-users');
                    $item->weight(3);
                    $item->append('admin.recruiting.candidate.create');
                    $item->route('admin.recruiting.candidate.index');
                    $item->authorize(
                        $this->auth->hasAccess('recruiting.candidates.index')
                    );
                });
// append



            });
        });

        return $menu;
    }
}
