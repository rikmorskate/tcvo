<?php namespace Modules\Recruiting\Entities;

use Illuminate\Database\Eloquent\Model;

class CandidateTranslation extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'recruiting__candidate_translations';
}
