<?php namespace Modules\Recruiting\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vacancy extends Model
{
    use SoftDeletes;
    use Translatable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'recruiting__vacancies';

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'title',
        'slug',
        'details',
        'requirements',
        'education',
        'meta_title',
        'meta_description',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'start_date',
        'end_date',
        'status',
        'street_name',
        'street_number',
        'street_number_suffix',
        'minor_municipality',
        'governing_district',
        'postal_area',
        'country',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $filters = [
        'type' => [
            'statement' => 'where_in_translation',
            'clue' => 'or',
        ]
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
//        'status' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * A vacancy can only belong to one organization
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo('\Modules\Recruiting\Entities\Organization');
    }

    /**
     * Set the governing district
     *
     * @param  string  $value
     * @return void
     */
    public function setGoverningDistrictAttribute($value)
    {
        $this->attributes['governing_district'] = strtolower($value);
    }
}
