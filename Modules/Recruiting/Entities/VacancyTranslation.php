<?php namespace Modules\Recruiting\Entities;

use Illuminate\Database\Eloquent\Model;

class VacancyTranslation extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'details',
        'requirements',
        'education',
        'meta_title',
        'meta_description',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'recruiting__vacancy_translations';
}
