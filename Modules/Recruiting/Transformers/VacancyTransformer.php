<?php namespace Modules\Recruiting\Transformers;

use Modules\Recruiting\Entities\Vacancy;
use League\Fractal;

class VacancyTransformer extends Fractal\TransformerAbstract
{
    public function transform(Vacancy $vacancy)
    {
        return [
            'id' => $vacancy->id,
            'title' => $vacancy->title,
            'details' => $vacancy->details,
            'requirements' => $vacancy->requirements,
            'education' => $vacancy->education,
            'type' => $vacancy->type,
            'status' => $vacancy->status,
            'street_name' => $vacancy->street_name,
            'street_number' => $vacancy->street_number,
            'street_number_suffix' => $vacancy->street_number_suffix,
            'minor_municipality' => $vacancy->minor_municipality,
            'governing_district' => $vacancy->governing_district,
            'postal_area' => $vacancy->postal_area,
            'country' => $vacancy->country,
            'start_date' => $vacancy->start_date != NULL ? $vacancy->start_date->toIso8601String() : NULL,
            'end_date' => $vacancy->end_date != NULL ? $vacancy->end_date->toIso8601String() : NULL,
//            'locale' => $vacancy->locale,
            'links' => [
                [
                    'rel' => 'self',
                    'uri' => '/api/recruiting/vacancies/' . $vacancy->id,
                ]
            ],
        ];
    }
}

