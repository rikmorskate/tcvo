<?php

return [
    'recruiting.vacancies' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
    ],
    'recruiting.candidates' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
    ],
    'recruiting.organizations' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
    ],
// append



];
