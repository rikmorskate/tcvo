<div class='form-group{{ $errors->has("street_name") ? ' has-error' : '' }}'>
    {!! Form::label("street_name", trans('recruiting::location.form.address')) !!}

    <div class="row">
        <div class="col-md-9 col-xs-8">
            {!! Form::text("street_name", Input::old('street_name'), ['id' => "street_name", 'data-slug' => 'source', 'class' => 'form-control', 'placeholder' => trans('recruiting::location.placeholder.street_name')]) !!}
        </div>
        <div class="col-md-2 col-xs-2">
            {!! Form::text("street_number", Input::old('street_name'), ['id' => "street_number", 'data-slug' => 'source', 'class' => 'form-control', 'placeholder' => trans('recruiting::location.placeholder.street_number')]) !!}
        </div>
        <div class="col-md-1 col-xs-2">
            {!! Form::text("street_number_suffix", Input::old('street_number_suffix'), ['id' => "street_number_suffix", 'data-slug' => 'source', 'class' => 'form-control', 'placeholder' => trans('recruiting::location.placeholder.street_number_suffix')]) !!}
        </div>
    </div>

    {!! $errors->first('street_name', '<span class="help-block">:message</span>') !!}
    {!! $errors->first('street_number', '<span class="help-block">:message</span>') !!}
    {!! $errors->first('street_number_suffix', '<span class="help-block">:message</span>') !!}
</div>
<div class='form-group{{ $errors->has("postal_area") ? ' has-error' : '' }}'>
    {!! Form::label("postal_area", trans('recruiting::location.form.postal_area')) !!}
    {!! Form::text("postal_area", Input::old("postal_area"), ['id' => "postal_area", 'class' => 'form-control', 'placeholder' => trans('recruiting::location.placeholder.postal_area')]) !!}
    {!! $errors->first("postal_area", '<span class="help-block">:message</span>') !!}
</div>
<div class='form-group{{ $errors->has("minor_municipality") ? ' has-error' : '' }}'>
    {!! Form::label("minor_municipality", trans('recruiting::location.form.minor_municipality')) !!}
    {!! Form::text("minor_municipality", Input::old("minor_municipality"), ['id' => "minor_municipality", 'class' => 'form-control', 'placeholder' => trans('recruiting::location.placeholder.minor_municipality')]) !!}
    {!! $errors->first("minor_municipality", '<span class="help-block">:message</span>') !!}
</div>
<div class='form-group{{ $errors->has("governing_district") ? ' has-error' : '' }}'>
    {!! Form::label("governing_district", trans('recruiting::location.form.governing_district')) !!}
    {!! Form::select('governing_district', [
        '' => '-----',
        'DR' => trans('recruiting::location.governing_districts.nl.dr'),
        'FL' => trans('recruiting::location.governing_districts.nl.fl'),
        'FR' => trans('recruiting::location.governing_districts.nl.fr'),
        'GE' => trans('recruiting::location.governing_districts.nl.ge'),
        'GR' => trans('recruiting::location.governing_districts.nl.gr'),
        'LI' => trans('recruiting::location.governing_districts.nl.li'),
        'NB' => trans('recruiting::location.governing_districts.nl.nb'),
        'NH' => trans('recruiting::location.governing_districts.nl.nh'),
        'OV' => trans('recruiting::location.governing_districts.nl.ov'),
        'UT' => trans('recruiting::location.governing_districts.nl.ut'),
        'ZE' => trans('recruiting::location.governing_districts.nl.ze'),
        'ZH' => trans('recruiting::location.governing_districts.nl.zh'),
    ], Input::old('governing_district'), ['id' => "country", 'class' => 'selectize']) !!}
    {!! $errors->first('governing_district', '<span class="help-block">:message</span>') !!}
</div>