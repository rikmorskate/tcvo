<div class='form-group{{ $errors->has("{$lang}.meta_title") ? ' has-error' : '' }}'>
    {!! Form::label("{$lang}[meta_title]", trans('recruiting::general.form.meta_title')) !!}
    {!! Form::text("{$lang}[meta_title]", Input::old("{$lang}.slug"), ['id' => "{$lang}[meta_title]", 'data-slug' => 'target', 'class' => 'form-control', 'placeholder' => trans('recruiting::general.placeholder.meta_title')]) !!}
    {!! $errors->first("{$lang}.meta_title", '<span class="help-block">:message</span>') !!}
</div>
<div class='form-group{{ $errors->has("{$lang}.meta_description") ? ' has-error' : '' }}'>
    {!! Form::label("{$lang}[meta_description]", trans('recruiting::general.form.meta_description')) !!}
    {!! Form::textArea("{$lang}[meta_description]", Input::old("{$lang}.meta_description"), ['id' => "{$lang}[meta_description]", 'class' => 'form-control editor', 'placeholder' => trans('recruiting::general.placeholder.meta_description')]) !!}
    {!! $errors->first("{$lang}.meta_description", '<span class="help-block">:message</span>') !!}
</div>