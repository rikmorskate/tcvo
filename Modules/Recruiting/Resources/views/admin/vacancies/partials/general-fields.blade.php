<div class='form-group{{ $errors->has("{$lang}.title") ? ' has-error' : '' }}'>
    {!! Form::label("{$lang}[title]", trans('recruiting::general.form.title').'*') !!}
    {!! Form::text("{$lang}[title]", Input::old('{$lang}.title'), ['id' => "{$lang}[title]", 'data-slug' => 'source', 'class' => 'form-control', 'placeholder' => trans('recruiting::general.placeholder.title')]) !!}
    {!! $errors->first('{$lang}.title', '<span class="help-block">:message</span>') !!}
</div>
<div class='form-group{{ $errors->has("{$lang}.slug") ? ' has-error' : '' }}'>
    {!! Form::label("{$lang}[slug]", trans('recruiting::general.form.slug').'*') !!}
    {!! Form::text("{$lang}[slug]", Input::old("{$lang}.slug"), ['id' => "{$lang}[slug]", 'data-slug' => 'target', 'class' => 'form-control', 'placeholder' => trans('recruiting::general.placeholder.slug')]) !!}
    {!! $errors->first("{$lang}.slug", '<span class="help-block">:message</span>') !!}
</div>
<div class='form-group{{ $errors->has("{$lang}.details") ? ' has-error' : '' }}'>
    {!! Form::label("{$lang}[details]", trans('recruiting::vacancies.form.details')) !!}
    {!! Form::textArea("{$lang}[details]", Input::old("{$lang}.details"), ['id' => "{$lang}[details]", 'class' => 'form-control editor', 'placeholder' => trans('recruiting::vacancies.placeholder.details')]) !!}
    {!! $errors->first("{$lang}.details", '<span class="help-block">:message</span>') !!}
</div>
<div class='form-group{{ $errors->has("{$lang}.requirements") ? ' has-error' : '' }}'>
    {!! Form::label("{$lang}[requirements]", trans('recruiting::vacancies.form.requirements')) !!}
    {!! Form::textArea("{$lang}[requirements]", Input::old("{$lang}.requirements"), ['id' => "{$lang}[requirements]", 'class' => 'form-control editor', 'placeholder' => trans('recruiting::vacancies.placeholder.requirements')]) !!}
    {!! $errors->first("{$lang}.requirements", '<span class="help-block">:message</span>') !!}
</div>
<div class='form-group{{ $errors->has("{$lang}.education") ? ' has-error' : '' }}'>
    {!! Form::label("{$lang}[education]", trans('recruiting::vacancies.form.education')) !!}
    {!! Form::select("{$lang}[education]", ['' => '-----','WO' => 'WO', 'HBO' => 'HBO', 'MBO' => 'MBO'], Input::old("{$lang}.education"), ['id' => "{$lang}[education]", 'class' => 'selectize']) !!}
    {!! $errors->first("{$lang}.education", '<span class="help-block">:message</span>') !!}
</div>