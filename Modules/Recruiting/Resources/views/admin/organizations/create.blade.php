@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('recruiting::organizations.title.create organization') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.recruiting.organization.index') }}">{{ trans('recruiting::organizations.title.organizations') }}</a></li>
        <li class="active">{{ trans('recruiting::organizations.title.create organization') }}</li>
    </ol>
@stop

@section('styles')
    {!! Theme::script('js/vendor/ckeditor/ckeditor.js') !!}
@stop

@section('content')
    {!! Form::open(['route' => ['admin.recruiting.organization.store'], 'method' => 'post']) !!}
    <div class="row">

        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#general"
                           data-toggle="tab">{{ trans('recruiting::general.tab.general') }}</a>
                    </li>
                    <li>
                        <a href="#details"
                           data-toggle="tab">{{ trans('recruiting::general.tab.details') }}</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="general">
                        <div class="row">
                            <div class="nav-tabs-custom">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="tab-content">
                                        @include('recruiting::admin.organizations.partials.general-fields')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="details">
                        <div class="row">
                            <div class="nav-tabs-custom">
                                @include('partials.tabs.languages', ['tab' => 'tab_'])
                                <div class="col-xs-12 col-sm-10 col-md-10">
                                    <div class="tab-content">
                                        <?php $i = 0; ?>
                                        @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                                            <?php $i++; ?>
                                            <div class="tab-pane {{ locale() == $locale ? 'active' : '' }} box-body"
                                                 id="tab_{{ $i }}">
                                                @include('recruiting::admin.organizations.partials.details-fields', ['lang' => $locale])
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="box-body right-menu-buttons">
                <div class="btn-group">
                    <button type="submit" value="back" name="back" class="btn btn-primary btn-flat">{{ trans('emixionlte::emixionlte.button.edit') }}</button>
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">{{ trans('core::core.button.toggle_dropdown') }}</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <button type="submit" name="new" value="new">{{ trans('emixionlte::emixionlte.button.edit_new') }}</button>
                        </li>
                        <li>
                            <button type="submit">{{ trans('emixionlte::emixionlte.button.edit_close') }}</button>
                        </li>
                        <li>
                            <button type="submit" name="clone" value="clone">{{ trans('emixionlte::emixionlte.button.edit_clone') }}</button>
                        </li>
                    </ul>
                </div>
                <div class="btn-group pull-right">
                    <a class="btn btn-danger btn-flat" href="{{ route('admin.recruiting.vacancy.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                </div>
            </div>
            <div class="box box-primary right-menu">
                <div class="box-body">
                    <div class='form-group'>
                        {!! Form::label('status', trans('recruiting::general.title.status'), array('style' => 'width:100%;')) !!}
                        {!! Form::select('status', [1 => trans('emixionlte::emixionlte.statuses.1'), 0 => trans('emixionlte::emixionlte.statuses.0')], 1, ['class' => 'form-control']) !!}
                    </div>

                    <div class='form-group'>
                        {!! Form::label('status', trans('core::core.table.created at'), array('style' => 'width:100%;')) !!}
                        -
                    </div>
                    <div class='form-group'>
                        {!! Form::label('status', trans('recruiting::general.title.updated at'), array('style' => 'width:100%;')) !!}
                        -
                    </div>
                </div>
            </div>
        </div>

    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.recruiting.organization.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            $('.selectize').selectize({
                delimiter: ',',
                plugins: ['remove_button']
            });
        });
    </script>
@stop
