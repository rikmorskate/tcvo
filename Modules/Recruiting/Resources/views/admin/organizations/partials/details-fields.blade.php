<div class='form-group{{ $errors->has("{$lang}.details") ? ' has-error' : '' }}'>
    {!! Form::label("{$lang}[details]", trans('recruiting::organizations.form.details')) !!}
    {!! Form::textArea("{$lang}[details]", Input::old("{$lang}.details"), ['id' => "{$lang}[details]", 'class' => 'form-control editor', 'placeholder' => trans('recruiting::organizations.placeholder.details')]) !!}
    {!! $errors->first("{$lang}.details", '<span class="help-block">:message</span>') !!}
</div>