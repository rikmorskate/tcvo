<?php

return [
    'tab' => [
    ],
    'table' => [
    ],
    'form' => [
        'address' => 'Address',
        'street_name' => 'Street name',
        'street_number' => 'Street number',
        'street_number_suffix' => 'Suffix',
        'minor_municipality' => 'City/town',
        'governing_district' => 'Governing district',
        'postal_area' => 'Postal code',
        'country' => 'Country',
    ],
    'placeholder' => [
        'address' => 'Address',
        'street_name' => 'Street name',
        'street_number' => 'Number',
        'street_number_suffix' => 'Suffix',
        'minor_municipality' => 'City/town',
        'governing_district' => 'Governing district',
        'postal_area' => 'Postal code',
        'country' => 'Country',
    ],
    'governing_districts' => [
        'nl' => [
            'dr' => 'Drenthe',
            'fl' => 'Flevoland',
            'fr' => 'Friesland',
            'ge' => 'Gelderland',
            'gr' => 'Groningen',
            'li' => 'Limburg',
            'nb' => 'Noord-Brabant',
            'nh' => 'Noord-Holland',
            'ov' => 'Overijssel',
            'ut' => 'Utrecht',
            'ze' => 'Zeeland',
            'zh' => 'Zuid-Holland',
        ]
    ]
];
