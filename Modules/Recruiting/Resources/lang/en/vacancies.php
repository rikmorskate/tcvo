<?php

return [
    'title' => [
        'vacancies' => 'Vacancies',
        'create vacancies' => 'Create a vacancies',
        'create vacancy' => 'Create a vacancy',
        'edit vacancies' => 'Edit a vacancies',
    ],
    'button' => [
        'create vacancies' => 'Create a vacancies',
    ],
    'table' => [
    ],
    'form' => [
        'details' => 'Details',
        'requirements' => 'Requirements',
        'education' => 'Education',
        'type' => 'Type',
    ],
    'placeholder' => [
        'details' => 'Details',
        'requirements' => 'Requirements',
        'education' => 'Education',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
