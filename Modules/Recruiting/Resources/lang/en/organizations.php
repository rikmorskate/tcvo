<?php

return [
    'title' => [
        'organizations' => 'Organizations',
        'create organizations' => 'Create a organizations',
        'edit organizations' => 'Edit a organizations',
    ],
    'button' => [
        'create organizations' => 'Create a organizations',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
