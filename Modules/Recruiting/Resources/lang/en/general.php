<?php

return [
    'tab' => [
        'general' => 'General',
        'location' => 'Location',
        'seo' => 'SEO',
    ],
    'table' => [
        'title' => 'Title',
        'name' => 'Name',
    ],
    'form' => [
        'title' => 'Title',
        'name' => 'Name',
        'slug' => 'Slug',
    ],
    'placeholder' => [
        'title' => 'Title',
        'name' => 'Name',
        'slug' => 'Slug',
    ],
];
