<?php

return [
    'title' => [
        'candidates' => 'Candidates',
        'create candidates' => 'Create a candidates',
        'edit candidates' => 'Edit a candidates',
    ],
    'button' => [
        'create candidates' => 'Create a candidates',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
