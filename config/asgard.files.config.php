<?php

return [
    'name' => 'Files',
    'root' => '/files',
    'pagination' => 12,
    'upload' => [
        'filesystem' => 'local',
        'allowed-types' => [
            '.jpg',
            '.png',
            '.gif',
            '.svg',
            '.psd',
            '.pdf',
            '.doc',
            '.docx',
            '.css',
            '.js',
            '.mp3',
            '.mp4',
            '.html',
            '.zip',
            '.exe',
            '.rar',
            '.txt',
            '.xls',
            '.xlsx'
        ],
        'max-file-size' => '200',
        'max-total-size' => 1000000000,
    ],
    'editor' => [
        'root' => '/editor',
        'rotation' => true,
        'overlay' => false // This is very intensive for the server.
    ]
];