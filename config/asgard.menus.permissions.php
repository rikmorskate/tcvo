<?php
return [
    'menus.menus' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
        'ajaxState'
    ],
    'menus.menuitem' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
    ],
];
