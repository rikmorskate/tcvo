<?php

return [
    'articles.categories' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
        'ajaxState'
    ],
    'articles.articles' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
        'ajaxState'
    ],
];
