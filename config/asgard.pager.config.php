<?php

return [
    'module_file' => 'pages.json',
    /*
    |--------------------------------------------------------------------------
    | Partials to include on page views
    |--------------------------------------------------------------------------
    | List the partials you wish to include on the different type page views
    | The content of those fields well be caught by the PageWasCreated and PageWasEdited events
    */
    'partials' => [
        'translatable' => [
            'create' => [],
            'edit' => [],
        ],
        'normal' => [
            'create' => [],
            'edit' => [],
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Dynamic relations
    |--------------------------------------------------------------------------
    | Add relations that will be dynamically added to the Page entity
    */
    'relations' => [
//        'extension' => function ($self) {
//            return $self->belongsTo(PageExtension::class, 'id', 'page_id')->first();
//        }
    ],
    /*
    |--------------------------------------------------------------------------
    | Array of middleware that will be applied on the page module front end routes
    |--------------------------------------------------------------------------
    */
    'middleware' => [],

    'sitemap' => [
        'enabled' => [
            'default' => true
        ],
        'change_frequency' => [
            'default' => 'weekly',
            'options' => [
                'always' => 'Always',
                'hourly' => 'Hourly',
                'daily' => 'Daily',
                'weekly' => 'Weekly',
                'monthly' => 'Monthly',
                'yearly' => 'Yearly',
                'never' => 'Never'
            ]
        ],
        'priority' => [
            'default' => 0.5,
            'options' => [
                '0.1' => 0.1,
                '0.2' => 0.2,
                '0.3' => 0.3,
                '0.4' => 0.4,
                '0.5' => 0.5,
                '0.6' => 0.6,
                '0.7' => 0.7,
                '0.8' => 0.8,
                '0.9' => 0.9,
                '1.0' => 1,
            ]
        ]
    ]
];