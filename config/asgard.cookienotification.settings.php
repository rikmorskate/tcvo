<?php

return [
    'text' => [
        'name' => 'text',
        'description' => 'cookienotification::cookies.title.text',
        'view' => 'wysiwyg',
        'translatable' => true
    ],
    'read_more_button' => [
        'description' => 'cookienotification::cookies.title.read_more_button',
        'view' => 'text',
        'translatable' => true
    ],
    'accept_button' => [
        'description' => 'cookienotification::cookies.title.accept_button',
        'view' => 'text',
        'translatable' => true,
        ''
    ],
    'read_more_page_url' => [
        'description' => 'cookienotification::cookies.title.read_more_page_url',
        'view' => 'text',
        'translatable' => true
    ]
];
