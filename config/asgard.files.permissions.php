<?php

return [
    'files.files' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
        'upload'
    ],
    'files.file' => [
        'upload',
        'download',
        'massdelete'
    ],
    'files.folders' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
    ],
    'files.categories' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
        'ajaxState'
    ],
    'files.overview' => [
        'overview',
        'changeParent',
        'discover',
        'selector'
    ]
];
