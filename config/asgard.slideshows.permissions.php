<?php

return [
    'slideshows.slideshows' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
    ],
    'slideshows.slides' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
    ],
// append


];
