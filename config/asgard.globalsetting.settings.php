<?php

return [
    'tabs' => [
        'seo' => [
            'name' => 'SEO instellingen'
        ],
        'website' => [
            'name' => 'Website',
            'translatable' => true,
        ]
    ],

    'Algemeen' => 'hr',

    'site:separator' => [
        'tab' => 'seo',
        'description' => 'Title seperator',
        'value' => ' | ',
        'view' => 'text'
    ],

    'site:meta_desc_max' => [
        'tab' => 'seo',
        'description' => 'globalsetting::settings.meta_desc_max',
        'value' => 150,
        'view' => 'number'
    ],

    'ua-code' => [
        'tab' => 'seo',
        'description' => 'UA Code',
        'view' => 'text',
        'placeholder' => 'UA-000000-01',
        'callback' => function (&$value) {
            if (!empty($value)) {
                $client_id = $value;
                $value = '(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');
    ga(\'create\', \'' . $client_id . '\', \'auto\');
    ga(\'send\', \'pageview\'); 
    ga(function(tracker) {window.clientId = tracker.get("clientId");});';
            }
        }
    ],
    'display-features' => [
        'tab' => 'seo',
        'description' => 'Display features',
        'view' => 'checkbox',
        'callback' => function (&$value) {
            if ((bool)$value) {
                $value = 'ga(\'require\', \'displayfeatures\');';
            }
        }
    ],
    'ecommerce' => [
        'tab' => 'seo',
        'description' => 'eCommerce',
        'view' => 'checkbox',
        'callback' => function (&$value) {
            if ((bool)$value) {
                $value = 'ga(\'require\', \'ecommerce\', \'ecommerce.js\');';
            }
        }
    ],

    'Twitter' => 'hr',

    'twitter:creator' => [
        'tab' => 'seo',
        'description' => 'Creator',
        'placeholder' => 'globalsetting::settings.twitter_account',
        'view' => 'text'
    ],

    'Facebook' => 'hr',

    'fb:admin' => [
        'tab' => 'seo',
        'description' => 'Admin',
        'view' => 'text'
    ],

    'fb:appid' => [
        'tab' => 'seo',
        'description' => 'App ID',
        'view' => 'text'
    ],


    'site:home-title' => [
        'tab' => 'website',
        'description' => 'Home title',
        'view' => 'text',
        'value' => '',
        'translatable' => true,
    ],

    'generator' => [
        'tab' => 'website',
        'description' => 'Generator',
        'view' => 'text',
        'value' => 'Emixion',
        'translatable' => true,
        'callback' => function (&$value) {
            if (!empty($value) && is_string($value)) {
                $value = '<meta name="generator" content="' . $value . '" />';
            }
        }
    ]
];