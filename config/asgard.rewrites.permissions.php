<?php

return [
    'rewrites.rewrites' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
        'ajaxState',
        'import'
    ],
];
