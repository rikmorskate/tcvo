<?php
return [
    'pager.pages' => [
        'index',
        'create',
        'store',
        'edit',
        'update',
        'destroy',
        'checkPagerFileChange',
        'ajaxState',
        'pages'
    ],
];
