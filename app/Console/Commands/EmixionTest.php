<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Globalsetting\Entities\Setting;
use Modules\Globalsetting\Facades\Settings;

class EmixionTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emixion:test {module?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rung Laravel tests';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $module = $this->argument('module');
        $output = '';
        $return_var = [];

        if (empty($module)) {
            exec('php vendor/phpunit/phpunit/phpunit', $output, $return_var);
        } else {
            exec('cd ' . base_path('Modules/' . $module) . ' && php ../../vendor/phpunit/phpunit/phpunit', $output, $return_var);
        }

        foreach ($output as $o) {
            echo $o . PHP_EOL;
        }
    }
}