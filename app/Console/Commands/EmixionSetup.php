<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Globalsetting\Entities\Setting;
use Pingpong\Modules\Facades\Module;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Modules\User\Permissions\PermissionManager;

class EmixionSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emixion:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup default Emixion CMS settings';

    private $permissions;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PermissionManager $permissionManager)
    {
        parent::__construct();
        $this->getLaravel()['env'] = 'local';
        $this->permissions = $permissionManager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Setup default settings
        $this->comment('Setup default settings');

        $this->call('clear-compiled');
        $this->call('module:publish');
        $this->call('stylist:publish');
        $this->call('vendor:publish');
        $this->call('migrate');

        Module::find('menu')->disable();
        Module::find('setting')->disable();
        Module::find('page')->disable();

        $this->call('module:migrate');
                
        Setting::where('name', 'core::template')->update(['plainValue' => 'Flixible']);
        Setting::where('name', 'core::locales')->update(['plainValue' => '["nl"]']);

        /* Create and setup roles of Emixion */
        $emixion_role = Sentinel::findRoleBySlug('emixion');
        if(empty($emixion_role)) {
            $emixion_role = Sentinel::getRoleRepository()->createModel()->create(['name' => 'Emixion', 'slug' => 'emixion']);
        }

        $permissions = array();
        foreach($this->permissions->all() as $permission) {
            if(empty($permission)) {
                continue;
            }

            foreach($permission as $index => $value) {
                foreach($value as $val) {
                    $permissions[$index . '.' . $val] = true;
                }
            }
        }

        $emixion_role->permissions = $permissions;
        $emixion_role->save();

        /* Set first account on role Emixion */
        $user = Sentinel::getUserRepository()->all()->first();

        $user->roles()->detach();
        $user->roles()->attach($emixion_role);
    }
}
